﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EbemySpawner : MonoBehaviour
{
    [SerializeField] private float _spawnTime = 1f;
    [SerializeField] private int _offset = 2;
    private Transform _player;

    private void Awake()
    {
        _player = GameObject.FindWithTag("Player").transform;
    }

    private void OnEnable()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private void OnDisable()
    {
        StopCoroutine(SpawnCoroutine());
    }

    IEnumerator SpawnCoroutine()
    {
        while(true)
        {
            var xPos = Random.Range(-_offset, _offset + 1);
            var yPos = Random.Range(-_offset, _offset + 1);

            PoolManager.Instance.EnemyList.GetFromPool(_player.position + new Vector3(xPos, yPos, 0), Quaternion.identity);
            yield return new WaitForSeconds(_spawnTime);
        }
    }
}
