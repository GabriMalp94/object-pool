﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Shoot(Vector3 dir)
    {
        _rigidbody.velocity = dir;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<IHittable>(out IHittable hit))
        {
            hit.hit();
        }
        PoolManager.Instance.BulletPool.AddInPool(this);
    }
}
