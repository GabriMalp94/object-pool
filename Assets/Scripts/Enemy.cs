﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IHittable
{
    public void hit()
    {
        Debug.Log("listing");
        PoolManager.Instance.EnemyList.AddInPool(this);
    }
}

public interface IHittable
{
    void hit();
}