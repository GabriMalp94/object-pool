﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager Instance { get; private set; }

    [SerializeField] Bullet bulletPrefab;
    ObjectPool<Bullet> _bulletPool = new ObjectPool<Bullet>();
    public ObjectPool<Bullet> BulletPool => _bulletPool;

    [SerializeField] Enemy _enemyPrefab;
    ObjectPool<Enemy> _enemyList = new ObjectPool<Enemy>();
    public ObjectPool<Enemy> EnemyList => _enemyList;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        BulletPool.InitPool(bulletPrefab);
        EnemyList.InitPool(_enemyPrefab);
    }
}
