﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class ObjectPool<T> : MonoBehaviour where T : Component
{
    private T _poolPrefab;
    private int _poolStartingsize = 10;

    private List<T> _pooList = new List<T>();

    public void InitPool(T prefab)
    {
        _poolPrefab = prefab;
        for(int i = 0; i < _poolStartingsize; i++)
        {
            var currentObject = Instantiate(_poolPrefab) as T;
            _pooList.Add(currentObject);
            currentObject.gameObject.SetActive(false);
        }
    }

    public T GetFromPool(Vector3 position, Quaternion rotation)
    {
        var currentObj = _pooList.FirstOrDefault();
        _pooList.Remove(currentObj);
        if(!currentObj)
        {
            currentObj = Instantiate(_poolPrefab) as T;
        }

        currentObj.transform.position = position;
        currentObj.transform.rotation = rotation;
        currentObj.gameObject.SetActive(true);

        return currentObj;
    }

    public void AddInPool(T poolObject)
    {
        poolObject.gameObject.SetActive(false);
        _pooList.Add(poolObject);
    }
}
